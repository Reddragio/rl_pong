"""
Experimental training procedure.
/!\ The main training procedure is in training.py
If you want to try this procedure, you need to use a pre-trained version of the agent named opponent.mdl
(So, just rename the given model.mdl to opponent.mdl)

Written by Jacques Charnay
"""

import gym
import numpy as np
import argparse
import wimblepong
from agent import Agent
import matplotlib.pyplot as plt
import time
import sys
from parallel_env_multiplayer import ParallelEnvs


def train(train_device="auto", model_path=None, train_timesteps=200000, update_steps=48, use_tensorboard=False):
    # We create the environments
    # This creates 64 parallel envs running in 8 processes (8 envs each)
    nb_processes = 8
    nb_envs_per_process = 8
    nb_envs = nb_processes * nb_envs_per_process
    env = ParallelEnvs("WimblepongVisualMultiplayer-v0", processes=nb_processes, envs_per_process=nb_envs_per_process)

    # We create the learning agent
    agent = Agent(train_device, nb_envs, use_tensorboard, print_infos=True)
    agent.load_model("opponent.mdl")   # We initialize it with the same IA as the opponent
    # We create the opponent agent
    opponent = Agent(train_device, nb_envs, print_infos=True)
    opponent.load_model("opponent.mdl")

    # Arrays to keep track of rewards
    reward_history, timestep_history = [], []
    average_reward_history = []

    # Run actual training
    # Reset the environment and observe the initial state
    observation, obs2 = env.reset()

    nb_update_steps = 1
    # Loop forever
    for timestep in range(train_timesteps):
        # Get action from the agent
        action, action_probabilities = agent.get_action_training(observation,
                                                                 epsilon_greedy_factor=1)  # 1-25000/(25000+timestep)
        previous_observation = observation
        act2 = opponent.get_action(obs2, parallel_mode=True)

        # Perform the action on the environment, get new state and reward
        observation_tuple, reward_tuple, done, info = env.step((action.cpu().detach().numpy(),
                                                                act2.cpu().detach().numpy()))
        observation, obs2 = observation_tuple
        reward, _ = reward_tuple

        for i in range(nb_envs):
            env_done = False
            # Check if the environment is finished; if so, store cumulative reward
            for envid, envreward in info["finished"]:
                if envid == i:
                    reward_history.append(envreward)
                    average_reward_history.append(np.mean(reward_history[-500:]))
                    env_done = True
                    break
            # Store action's outcome (so that the agent can improve its policy)
            agent.store_outcome(previous_observation[i], observation[i],
                                action_probabilities[i], reward[i], env_done, action[i], i)

            if env_done:
                opponent.reset(i)

        if timestep % update_steps == update_steps-1:
            print(f"Update {nb_update_steps} - timestep {timestep+1}")
            agent.update_policy(mini_batch_size=nb_envs, nb_epochs=5)
            nb_update_steps += 1

    agent.save_model(model_path)
    print("Model saved.")

    plt.plot(reward_history)
    plt.plot(average_reward_history)
    plt.legend(["Reward", "500-episodes average"])
    plt.title("Reward history - Parallel PPO Actor-Critic")
    plt.savefig("reward_history.png", dpi=300)
    plt.show()
    print("Training finished.")

    # Close environments processes
    time.sleep(0.5)  # Just sleep to have messages in good order in the shell
    env.stop()
    # Close tensorboard logging, if necessary
    agent.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--train_device", type=str, default="auto", help="Train device (cpu or cuda) - if 'auto', "
                                                                         "choose cuda if available, cpu otherwise")
    parser.add_argument("--save_model", type=str, default=None, help="Name of the file where the model is saved")
    parser.add_argument("--train_timesteps", type=int, default=1000, help="Number of parallel train timesteps")
    parser.add_argument("--update_steps", type=int, default=48, help="Number of timesteps before updating the policy")
    parser.add_argument("--tensorboard", action='store_true', help="Use tensorboard to study training history")
    args = parser.parse_args()

    start_time = time.time()
    train(args.train_device, args.save_model, args.train_timesteps, args.update_steps, args.tensorboard)
    execution_time = time.time() - start_time
    print("Execution time:")
    print('{:.2f} seconds'.format(execution_time))
    print('{:.2f} hours'.format(execution_time / 3600))
