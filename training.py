"""
Main training procedure.
You can run it without any parameters and it will run with the default parameters described in the report.

Written by Jacques Charnay & Peyman Peirovifar
"""

import gym
import numpy as np
import argparse
import wimblepong
from agent import Agent
import matplotlib.pyplot as plt
import time
import sys
from parallel_env import ParallelEnvs
import tempfile
import shutil
import os


def train(train_device="auto", model_path=None, train_timesteps=200000, update_steps=48, use_tensorboard=False,
          show_plot=False):
    # We create the environments
    # This creates 64 parallel envs running in 8 processes (8 envs each)
    nb_processes = 8
    nb_envs_per_process = 8
    nb_envs = nb_processes * nb_envs_per_process
    env = ParallelEnvs("WimblepongVisualSimpleAI-v0", processes=nb_processes, envs_per_process=nb_envs_per_process)

    # We create the agent
    agent = Agent(train_device, nb_envs, use_tensorboard, print_infos=True)

    # Arrays to keep track of rewards
    reward_history, timestep_history = [], []
    average_reward_history = []

    # Run actual training
    # Reset the environment and observe the initial state
    observation = env.reset()

    # Clever stop variables
    update_best_model_frequency = 50  # Try to update the best model all 50 games
    minimal_performance = 5.0  # starts saving models if they achieve at least this level of performance
    best_performance_observed = -10.0
    best_model_found = False
    file_descriptor, path_best_model = tempfile.mkstemp()
    best_model = open(path_best_model, 'wb')

    nb_update_steps = 1
    # Loop forever
    for timestep in range(train_timesteps):
        # Get action from the agent
        action, action_probabilities = agent.get_action_training(observation)
        previous_observation = observation

        # Perform the action on the environment, get new state and reward
        observation, reward, done, info = env.step(action.cpu().detach().numpy())

        for i in range(nb_envs):
            env_done = False
            # Check if the environment is finished; if so, store cumulative reward
            for envid, envreward in info["finished"]:
                if envid == i:
                    reward_history.append(envreward)
                    average_reward_history.append(np.mean(reward_history[-200:]))

                    if len(average_reward_history) % update_best_model_frequency == 0 \
                            and average_reward_history[-1] >= minimal_performance:
                        if average_reward_history[-1] > best_performance_observed:
                            best_performance_observed = average_reward_history[-1]
                            best_model.seek(0)
                            best_model.truncate()
                            agent.save_model(best_model)
                            best_model_found = True

                    env_done = True
                    break
            # Store action's outcome (so that the agent can improve its policy)
            agent.store_outcome(previous_observation[i], observation[i],
                                action_probabilities[i], reward[i], env_done, action[i], i)

        if timestep % update_steps == update_steps-1:
            print(f"Update {nb_update_steps} - timestep {timestep+1}")
            agent.update_policy(mini_batch_size=nb_envs, nb_epochs=5)
            nb_update_steps += 1

    best_model.close()
    os.close(file_descriptor)
    if not best_model_found:
        agent.save_model(model_path)
        print("Model saved. Minimum performance not achieved...")
    else:
        shutil.copy(path_best_model, model_path)
        os.remove(path_best_model)
        print("Model saved. Theoretical performance level: "+str(best_performance_observed))

    plt.plot(reward_history)
    plt.plot(average_reward_history)
    plt.legend(["Reward", "200-episodes average"])
    plt.title("Reward history - Parallel PPO Actor-Critic")
    plt.savefig("reward_history.png", dpi=300)
    if show_plot:
        plt.show()
    print("Training finished.")

    # Close environments processes
    env.stop()
    # Close tensorboard logging, if necessary
    agent.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--train_device", type=str, default="auto", help="Train device (cpu or cuda) - if 'auto', "
                                                                         "choose cuda if available, cpu otherwise")
    parser.add_argument("--save_model", type=str, default="model.mdl", help="Name of the file where the model is saved")
    parser.add_argument("--train_timesteps", type=int, default=200000, help="Number of parallel train timesteps")
    parser.add_argument("--update_steps", type=int, default=48, help="Number of timesteps before updating the policy")
    parser.add_argument("--tensorboard", action='store_true', help="Use tensorboard to study training history")
    parser.add_argument("--show_plot", action='store_true', help="Show reward history plot at the end of training")
    args = parser.parse_args()

    start_time = time.time()
    train(args.train_device, args.save_model, args.train_timesteps, args.update_steps, args.tensorboard, args.show_plot)
    execution_time = time.time() - start_time
    print("Execution time:")
    print('{:.2f} seconds'.format(execution_time))
    print('{:.2f} hours'.format(execution_time / 3600))
