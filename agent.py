"""
PPO Actor-Critic Agent

Written by Jacques Charnay & Peyman Peirovifar
"""

import torch
import cv2
from torch.utils.tensorboard import SummaryWriter
import torch.nn.functional as F


class Policy(torch.nn.Module):
    def __init__(self, state_space, action_space, nb_stacked_frames):
        super().__init__()
        # For Pong game:
        # state_space = (200, 200, 3)
        # (But currently we used only resized red component so the state_space is (100, 100))
        # action_space = 3 (possibles actions --> 0: STAY, 1: UP, 2: DOWN)
        self.state_space = state_space
        self.action_space = action_space
        self.hidden = 512
        self.nb_input_channel = nb_stacked_frames  # We take 3 consecutive frames as input

        # Convolutional layers
        self.conv1 = torch.nn.Conv2d(in_channels=self.nb_input_channel, out_channels=16, kernel_size=8, stride=2,
                                     padding=1)
        self.relu1 = torch.nn.ReLU()  # Because of stride=2, the picture dimension is now (48,48)
        self.conv2 = torch.nn.Conv2d(in_channels=16, out_channels=32, kernel_size=3, stride=2, padding=1)
        self.relu2 = torch.nn.ReLU()  # Because of stride=2, the picture dimension is now (24,24)

        # Common fully connected layer
        self.nb_input_fc = 32 * 24 * 24
        self.fc = torch.nn.Linear(self.nb_input_fc, self.hidden)
        self.relu4 = torch.nn.ReLU()

        # Actor layers
        self.fc2 = torch.nn.Linear(self.hidden, 256)
        self.relu5 = torch.nn.ReLU()
        self.actor = torch.nn.Linear(256, self.action_space)
        self.log_softmax = torch.nn.LogSoftmax(dim=1)
        # Critic layer
        self.fc3 = torch.nn.Linear(self.hidden, 256)
        self.relu6 = torch.nn.ReLU()
        self.critic = torch.nn.Linear(256, 1)

        self.init_weights()

    def init_weights(self):
        # Init weights associated with all layers
        for m in self.modules():
            if type(m) is torch.nn.Linear:
                torch.nn.init.xavier_uniform_(m.weight)
                torch.nn.init.zeros_(m.bias)
            elif type(m) is torch.nn.Conv2d:
                torch.nn.init.xavier_uniform_(m.weight)
                torch.nn.init.zeros_(m.bias)

    def reset(self):
        self.init_weights()

    def forward(self, x):
        # Convolutional layers
        x = self.conv1(x)
        x = self.relu1(x)
        x = self.conv2(x)
        x = self.relu2(x)
        # We need to flatten the tensors before the fully connected layers
        x = x.view(x.size(0), -1)
        # Fully connected common layer
        x = self.fc(x)
        x = self.relu4(x)

        # Actor layers
        y = self.fc2(x)
        y = self.relu5(y)
        y = self.actor(y)
        action_probs = self.log_softmax(y)
        # Critic layer
        y = self.fc3(x)
        y = self.relu6(y)
        state_values = self.critic(y).squeeze()

        return action_probs, state_values


class Agent(object):
    def __init__(self, train_device="auto", nb_envs=1, use_tensorboard=False, print_infos=False):
        self.train_device = train_device
        if train_device == "auto":
            if torch.cuda.is_available():
                self.train_device = "cuda"
            else:
                self.train_device = "cpu"
        if print_infos:
            print("Training model on", self.train_device)

        # The state space is composed of 200*200 pixels RGB images
        self.observation_space_dim = (100, 100)  # We keep only the red component of color.
        # (The size of image is reduced by a factor of 2)
        # 3 possibles actions --> 0: STAY, 1: UP, 2: DOWN
        self.action_space_dim = 3
        # We create the policy
        self.nb_stacked_frames = 3
        self.policy = Policy(self.observation_space_dim, self.action_space_dim, self.nb_stacked_frames).to(self.train_device)

        self.learning_rate = 2e-4
        self.optimizer = torch.optim.Adam(self.policy.parameters(), lr=self.learning_rate)
        self.gamma = 0.99  # We give priority to actions that make us win quickly
        self.clip_param = 0.2
        self.loss_critic_coeff = 0.5
        self.entropy_coeff = 0.01
        self.epsilon_greedy = 1

        # Lists to store episode data
        self.history_states = []
        self.history_next_states = []
        self.history_action_probs = []
        self.history_rewards = []
        self.history_done = []
        self.history_actions = []

        self.nb_envs = nb_envs
        self.blank_frame = torch.zeros(self.observation_space_dim).float().to(self.train_device)
        # We always save last observations, to use successive frames as input
        self._initialize_last_observations()

        self.use_tensorboard = use_tensorboard
        self.tb_writer = None
        self.tb_step = 0
        if self.use_tensorboard:
            self.tb_writer = SummaryWriter()

    def _initialize_last_observations(self):
        self.last_observations = []
        for i in range(self.nb_envs):
            self.last_observations.append([])
            for j in range(self.nb_stacked_frames + 1):
                # We always save nb_stacked_frames + the next frame, to simplify stack calculations
                self.last_observations[i].append(self.blank_frame)
        self.first_frame = []
        for i in range(self.nb_envs):
            self.first_frame.append(True)

    def _reinitialize_env_last_observation(self, env_id):
        for j in range(self.nb_stacked_frames + 1):
            # We always save nb_stacked_frames + the next frame, to simplify stack calculations
            self.last_observations[env_id][j] = self.blank_frame
        self.first_frame[env_id] = True

    def close(self):
        # Close tensorboard logging, if necessary
        if self.use_tensorboard:
            self.tb_writer.close()

    def update_policy(self, mini_batch_size=512, nb_epochs=15):
        # Transform history into Torch tensors
        states = torch.stack(self.history_states, dim=0).to(self.train_device).squeeze(-1)
        next_states = torch.stack(self.history_next_states, dim=0).to(self.train_device).squeeze(-1)
        old_act_log_probs = torch.stack(self.history_action_probs, dim=0).to(self.train_device).squeeze(-1)
        rewards = torch.stack(self.history_rewards, dim=0).to(self.train_device).squeeze(-1)
        done = torch.Tensor(self.history_done).to(self.train_device)
        actions = torch.stack(self.history_actions, dim=0).to(self.train_device).squeeze(-1)

        # Reinitialize episodes data
        self.history_states = []
        self.history_next_states = []
        self.history_action_probs = []
        self.history_rewards = []
        self.history_done = []
        self.history_actions = []

        data_size = states.size(0)
        for i in range(nb_epochs):
            for j in range(0, data_size, mini_batch_size):
                batch_states = states[j:(j+mini_batch_size)]
                batch_next_states = next_states[j:(j+mini_batch_size)]
                batch_old_act_log_probs = old_act_log_probs[j:(j+mini_batch_size)]
                batch_rewards = rewards[j:(j+mini_batch_size)]
                batch_done = done[j:(j+mini_batch_size)]
                batch_actions = actions[j:(j+mini_batch_size)]

                batch_all_act_log_probs, state_values = self.policy.forward(batch_states)
                batch_act_log_probs = torch.gather(batch_all_act_log_probs, 1, batch_actions[:, None]).squeeze()
                # The value of the terminal state is 0 thanks to (1 - done)
                _, state_value_next = self.policy.forward(batch_next_states)
                state_value_next = state_value_next * (1 - batch_done)

                # Critic loss (MSE)
                target = batch_rewards + self.gamma * state_value_next.detach()
                loss_critic = F.mse_loss(state_values, target)

                # Advantage estimates
                advantage = (batch_rewards + self.gamma * state_value_next - state_values).detach()
                # Ratio between theta and theta_old
                ratio = torch.exp(batch_act_log_probs - batch_old_act_log_probs.detach())
                # basic surrogate and clipped surrogate
                surrogate1 = ratio * advantage
                surrogate2 = torch.clamp(ratio, 1.0 - self.clip_param, 1.0 + self.clip_param) * advantage
                # Final actor loss
                loss_actor = -torch.min(surrogate1, surrogate2).mean()

                # Entropy of the probabilities distribution
                entropy = -torch.sum(torch.exp(batch_all_act_log_probs) * batch_all_act_log_probs, dim=1)
                entropy = entropy.mean()

                # Global loss
                global_loss = self.loss_critic_coeff * loss_critic + loss_actor - self.entropy_coeff * entropy

                if self.use_tensorboard:
                    self.tb_writer.add_scalar("loss_critic", loss_critic, self.tb_step)
                    self.tb_writer.add_scalar("loss_actor", loss_actor, self.tb_step)
                    self.tb_writer.add_scalar("entropy", -entropy, self.tb_step)
                    self.tb_writer.add_scalar("global_loss", global_loss, self.tb_step)
                    self.tb_step += 1

                # Update policy thanks to ADAM optimizer
                global_loss.backward()
                self.optimizer.step()
                self.optimizer.zero_grad()

    def get_action_training(self, observation, evaluation=False, epsilon_greedy_factor=1):
        # Here, observation is an array of observations
        states = []
        for i in range(self.nb_envs):
            if self.first_frame[i] or evaluation:
                state_tensor = self._preprocess_frame(observation[i])
                self.last_observations[i][self.nb_stacked_frames] = state_tensor
                if self.first_frame[i]:
                    for j in range(self.nb_stacked_frames):
                        self.last_observations[i][j] = state_tensor
                    self.first_frame[i] = False
            state = torch.stack(self.last_observations[i][1:(self.nb_stacked_frames+1)])
            states.append(state)
        states = torch.stack(states)

        with torch.no_grad():
            # We obtain the probabilities for the 3 actions:
            all_action_log_probs, _ = self.policy.forward(states)
            # We select the actions with the highest probabilities or a random action with probability epsilon
            # NB : CURRENTLY, epsilon greedy action selection is not used in the standard training procedure.
            #      We keep the code to eventually use it later on
            epsilon = self.epsilon_greedy * (1 - epsilon_greedy_factor)  # CURRENTLY, epsilon = 0
            epsilon_probs = torch.rand(self.nb_envs).to(self.train_device)
            best_mask = (epsilon_probs >= epsilon).type(torch.int64).to(self.train_device)
            best_actions = torch.argmax(all_action_log_probs, dim=1)
            greedy_mask = (epsilon_probs < epsilon).type(torch.int64).to(self.train_device)
            greedy_probs = torch.rand(self.nb_envs).to(self.train_device)
            greedy_actions = (greedy_probs > 1/3).type(torch.int64).to(self.train_device) + \
                             (greedy_probs > 2/3).type(torch.int64).to(self.train_device)
            action = best_mask * best_actions + greedy_mask * greedy_actions
            # We finally select the corresponding probabilities:
            action_log_probs = torch.gather(all_action_log_probs, 1, action[:, None]).squeeze()

        return action, action_log_probs

    def get_action(self, frame, parallel_mode=False):
        if not parallel_mode:
            # Here, observation is a unique observation
            action, _ = self.get_action_training(frame[None, :], evaluation=True)
            action = int(action[0])
        else:
            action, _ = self.get_action_training(frame, evaluation=True)

        # In evaluation mode, we need to do here the shift of last observations
        for i in range(self.nb_envs):
            for j in range(self.nb_stacked_frames):
                self.last_observations[i][j] = self.last_observations[i][j + 1]

        return action

    def store_outcome(self, observation, next_observation, action_prob, reward, done, action, env_id):
        for j in range(self.nb_stacked_frames):
            self.last_observations[env_id][j] = self.last_observations[env_id][j+1]
        next_state_tensor = self._preprocess_frame(next_observation)
        self.last_observations[env_id][self.nb_stacked_frames] = next_state_tensor
        # (Observation state is calculated in get_training)

        state = torch.stack(self.last_observations[env_id][:self.nb_stacked_frames])
        next_state = torch.stack(self.last_observations[env_id][1:(self.nb_stacked_frames+1)])

        self.history_states.append(state)
        self.history_next_states.append(next_state)
        self.history_action_probs.append(action_prob)
        self.history_rewards.append(torch.Tensor([reward]))
        self.history_done.append(done)
        self.history_actions.append(action)

        if done:
            self._reinitialize_env_last_observation(env_id)

    def _preprocess_frame(self, frame):
        frame = cv2.resize(frame, self.observation_space_dim)
        frame = frame[:, :, 0]  # We keep only the red component of color
        frame = (frame > 33).astype(float)  # We pass the image in black and white
        return torch.from_numpy(frame).float().to(self.train_device)

    def load_model(self, path=None):
        if path is None:
            path = "model.mdl"
        model_backup = torch.load(path, map_location=self.train_device)
        self.policy.load_state_dict(model_backup)

    def save_model(self, path=None):
        if path is None:
            path = "model.mdl"
        torch.save(self.policy.state_dict(), path)

    def complete_reset(self):
        # Reset policy weights
        self.policy.reset()
        # Reset optimizer
        self.optimizer = torch.optim.Adam(self.policy.parameters(), lr=self.learning_rate)
        # Reset last observation
        self._initialize_last_observations()

        # Reinitialize episodes data
        self.history_states = []
        self.history_next_states = []
        self.history_action_probs = []
        self.history_rewards = []
        self.history_done = []
        self.history_actions = []

    def reset(self, env_id=0):
        self.first_frame[env_id] = True

    @staticmethod
    def get_name():
        return "jojo"
